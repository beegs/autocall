package com.fragment;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.autocall.MainActivity;
import com.autocall.R;
import com.bean.CallData;
import com.utils.AccelerometerListener;

public class OtherFragment extends Fragment implements OnClickListener{
	
	private EditText et_startnum;
	private EditText et_stopnum;
	private EditText et_delay;
	private EditText et_interval;
	private Button btnCall;
	private EditText et_coefficient;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_other, null);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		et_startnum = (EditText) view.findViewById(R.id.start_num);
        et_stopnum = (EditText) view.findViewById(R.id.end_num);
        et_delay = (EditText) view.findViewById(R.id.endcall_time);
        et_interval = (EditText) view.findViewById(R.id.call_space);
        et_coefficient = (EditText) view.findViewById(R.id.coefficient);
		
		btnCall = (Button) view.findViewById(R.id.start_button);
		btnCall.setOnClickListener(this);
        btnCall.setTag("0");
        
        SharedPreferences userInfo = getActivity().getSharedPreferences("userinfo", 0);
        et_coefficient.setText(userInfo.getString("coefficient_other", AccelerometerListener.coefficient + ""));
	}

	public static boolean isCellphone(String str) {
        try {
            Long.parseLong(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
	 
	 private void showDialog(String text){
		 AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		 builder.setTitle("提示");
		 builder.setMessage(text);
		 builder.setPositiveButton("确定", null);
		 builder.show();
	 }
	 
	 private int formatTime(String num){
		 int data = -1;
		 try {
			 data = Integer.parseInt(num);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		 
		 return data;
	 }



	@Override
	public void onClick(View v) {
		if (btnCall.getTag().equals("0")) {
			CallData callData = new CallData();
			callData.phoneNum = et_startnum.getText().toString().trim();
			if (!isCellphone(callData.phoneNum)){
				showDialog("输入的号码不合法");
				return;
			}

            callData.endPhoneNum = et_stopnum.getText().toString().trim();
            if (!isCellphone(callData.phoneNum)){
                showDialog("输入的号码不合法");
                return;
            }

			String delay = et_delay.getText().toString().trim();
			callData.delay = formatTime(delay);
			if (callData.delay == -1)
			{
				showDialog("输入的挂断延时不合法");
				return;
			}
			
			String interval = et_interval.getText().toString().trim();
			callData.interval = formatTime(interval);
			if (callData.interval == -1)
			{
				showDialog("输入的拨号间隔不合法");
				return;
			}

			int coefficient = formatTime(et_coefficient.getText().toString().trim());
			if (coefficient < 0) {
				showDialog("震动系数不合法");
				return;
			}
			AccelerometerListener.coefficient = coefficient;
			
			// baocun
			SharedPreferences userInfo = getActivity().getSharedPreferences("userinfo", 0);
	        userInfo.edit().putString("coefficient_other", coefficient + "").commit();
	        
			
            callData.type = 2;
            callData.isRepeat = true;
			
			btnCall.setText("停止拨打");
			btnCall.setTag("1");
			((MainActivity)getActivity()).getCallService().startCall(callData);
		}
		else
		{
			btnCall.setText("开始拨打");
			btnCall.setTag("0");
			((MainActivity)getActivity()).getCallService().destoryCall();
		}
	}

    public void destory()
    {
        btnCall.setText("开始拨打");
        btnCall.setTag("0");
    }
}
