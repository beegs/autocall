package com.fragment;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.autocall.MainActivity;
import com.autocall.R;
import com.bean.CallData;
import com.utils.AccelerometerListener;

public class CallPhoneFragment extends Fragment implements OnClickListener {
	private EditText et_callnum;
	private EditText et_delay;
	private EditText et_interval;
	private EditText et_number; 
	private CheckBox checkbox;
	private Button btnCall;
	private EditText et_coefficient;
	
	public static ArrayList<String> nums;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		readNums();
		return inflater.inflate(R.layout.fragment_allphone, null);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		et_callnum = (EditText) view.findViewById(R.id.phone_num);
		et_delay = (EditText) view.findViewById(R.id.endcall_time);
		et_interval = (EditText) view.findViewById(R.id.call_space);
		et_number = (EditText) view.findViewById(R.id.call_time);
		et_coefficient = (EditText) view.findViewById(R.id.coefficient);
		
		checkbox = (CheckBox) view.findViewById(R.id.checkbox);
		btnCall = (Button) view.findViewById(R.id.start_button);

		btnCall.setTag("0");
		btnCall.setOnClickListener(this);

        SharedPreferences userInfo = getActivity().getSharedPreferences("userinfo", 0);
        //et_callnum.setText(userInfo.getString("num", ""));
        
        et_coefficient.setText(userInfo.getString("coefficient", AccelerometerListener.coefficient + ""));
	
        Button btn_add = (Button) view.findViewById(R.id.add);
        Button btn_clear = (Button) view.findViewById(R.id.clear);
        
        btn_add.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				nums.add(et_callnum.getText().toString().trim());
				saveNums();
				et_callnum.setText("");
				Toast.makeText(getActivity(), "保存成功", Toast.LENGTH_SHORT).show();;
			}
		});
        
        btn_clear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				nums.clear();
				saveNums();
				Toast.makeText(getActivity(), "清除成功", Toast.LENGTH_SHORT).show();;
			}
		});
	}

	 public static boolean isMobileNO(String mobiles) {
	        Pattern p = Pattern
	                .compile("^((13[0-9])|(15[^4,//D])|(18[0,5-9]))//d{8}$");
	        Matcher m = p.matcher(mobiles);
	        System.out.println(m.matches() + "---");
	        return m.matches();
	    }

	public static boolean isCellphone(String str) {
		Pattern pattern = Pattern.compile("1[0-9]{10}");
		Matcher matcher = pattern.matcher(str);
		if (matcher.matches()) {
			return true;
		} else {
			return false;
		}
	}
	 
	 private void showDialog(String text){
		 AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		 builder.setTitle("提示");
		 builder.setMessage(text);
		 builder.setPositiveButton("确定", null);
		 builder.show();
	 }
	 
	 private int formatTime(String num){
		 int data = -1;
		 try {
			 data = Integer.parseInt(num);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		 
		 return data;
	 }
	 
	@Override
	public void onClick(View v) {
		if (btnCall.getTag().equals("0")) {
			CallData callData = new CallData();
			if (nums.isEmpty()){
				showDialog("请添加号码");
				return;
			}

            String delay = et_delay.getText().toString().trim();
			callData.delay = formatTime(delay);
			if (callData.delay == -1)
			{
				showDialog("输入的挂断延时不合法");
				return;
			}

			String interval = et_interval.getText().toString().trim();
			callData.interval = formatTime(interval);
			if (callData.interval == -1)
			{
				showDialog("输入的拨号间隔不合法");
				return;
			}
			
			callData.isRepeat = checkbox.isChecked();
			String number = et_number.getText().toString().trim();
			callData.number = formatTime(number);
			if (callData.number == -1 && !callData.isRepeat)
			{
				showDialog("输入的拨打次数不合法");
				return;
			}

			callData.type = 1;
			
			int coefficient = formatTime(et_coefficient.getText().toString().trim());
			if (coefficient < 0) {
				showDialog("震动系数不合法");
				return;
			}
			AccelerometerListener.coefficient = coefficient;
			
			// baocun
			SharedPreferences userInfo = getActivity().getSharedPreferences("userinfo", 0);
	        userInfo.edit().putString("num", callData.phoneNum).commit();
	        userInfo.edit().putString("coefficient", coefficient + "").commit();
			
			btnCall.setText("停止拨打");
			btnCall.setTag("1");
			((MainActivity)getActivity()).getCallService().startCall(callData);
		}
		else
		{
			btnCall.setText("开始拨打");
			btnCall.setTag("0");
			((MainActivity)getActivity()).getCallService().destoryCall();
		}

	}

	public void destory()
	{
		btnCall.setText("开始拨打");
		btnCall.setTag("0");
	}
	
	private static ObjectOutputStream      oos             = null;
    @SuppressLint("InlinedApi")
    private static String getSaveData(Object object)
    {
        ByteArrayOutputStream   baos            = null;
        String                  data            = null;
        
        baos = new ByteArrayOutputStream();
        try
        {
            oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
            data = new String(Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT));
            oos.flush();
            oos.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        
        return data;
    }
    
    private void saveNums() {
    	SharedPreferences  mySharedPreferences  =  getActivity().getSharedPreferences("base64", getActivity().MODE_PRIVATE);
        SharedPreferences.Editor editor = mySharedPreferences.edit();
        
        // 存储Roster机构树
        String data = getSaveData(nums);
        editor.putString("nums", data);
        editor.commit();
    }
    
    @SuppressWarnings("unchecked")
	private void readNums() {
    	try
        {
            byte[]                  base64Bytes     = null;
            ByteArrayInputStream    bais            = null;
            String                  productBase64   = null;
            ObjectInputStream       ois             = null;
            SharedPreferences mySharedPreferences   = getActivity().getSharedPreferences("base64", Activity.MODE_PRIVATE);
            
            // 读取Group机构树的数据
            productBase64   = mySharedPreferences.getString("nums", null);
            if (productBase64 != null) {
              base64Bytes     = Base64.decode(productBase64, Base64.DEFAULT);
              bais            = new ByteArrayInputStream(base64Bytes); 
              ois             = new ObjectInputStream(bais);
              nums = (ArrayList<String>) ois.readObject();
              ois.close();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    	finally {
    		if (nums == null)
    		nums = new ArrayList<String>();
    	}
    	
    }
}
