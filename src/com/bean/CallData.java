package com.bean;

import java.io.Serializable;
import java.util.ArrayList;

public class CallData implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public int type;
	public String phoneNum;
	public int delay;
	public int interval;
	public int number;
	public boolean isRepeat;
	public ArrayList<String> nums = new ArrayList<String>();

    //
    public String endPhoneNum;
}
