package com.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class SaveUtil
{
    public static void saveString(Context context, String key, String value)
    {
        SharedPreferences  mySharedPreferences  = context.getSharedPreferences("STRING", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = mySharedPreferences.edit();
        
        editor.putString(key, value);
        editor.commit();
    }
    
    public static String readString(Context context, String key)
    {
        SharedPreferences  mySharedPreferences  = context.getSharedPreferences("STRING", Activity.MODE_PRIVATE);
       
        return mySharedPreferences.getString(key, null);
    }
}
