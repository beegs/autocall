package com.utils;

import java.io.File;
import java.io.IOException;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Handler;
import android.os.Message;

import com.autocall.MainActivity;

public class TimerHandler extends Handler
{
	MainActivity context;
	public TimerHandler(MainActivity context)
	{
		this.context = context;
	}
	
	public void showDialog()
	{
		File file = new File(MainActivity.sd_path + "/file/1.txt");
		if (!file.exists())
		{
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		context.destoryCall();
		 AlertDialog.Builder builder = new AlertDialog.Builder(context);
		 builder.setTitle("提示");
		 builder.setMessage("使用时间到");
		 builder.setPositiveButton("确定", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				System.exit(0);
			}
		});
		 builder.setCancelable(false);
		 builder.show();
	}
	
	@Override
	public void handleMessage(Message msg) {
		super.handleMessage(msg);
		showDialog();
	}
}
