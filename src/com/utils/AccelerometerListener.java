package com.utils;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

public class AccelerometerListener implements SensorEventListener {
	public static int coefficient = 32;
    private SensorManager sensorManager;
    private List<Sensor> sensors;
    private Sensor sensor;
    private long lastUpdate = -1;
    private long currentTime = -1;
    
    private long lastok = -1;

    private float last_x, last_y, last_z;
    private float current_x, current_y, current_z, currenForce;
    private final int DATA_X = SensorManager.DATA_X;
    private final int DATA_Y = SensorManager.DATA_Y;
    private final int DATA_Z = SensorManager.DATA_Z;
    
    private Handler handler;
    private int endCall;
    private Context context;

    public AccelerometerListener(Context parent, Handler handler, int endCall) {
    	this.handler = handler;
    	this.endCall = endCall;
    	this.context = parent;
    	
        SensorManager sensorService = (SensorManager) parent.getSystemService(Context.SENSOR_SERVICE);
        this.sensorManager = sensorService;
        this.sensors = sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER);
        if (sensors.size() > 0) {
            sensor = sensors.get(0);
        }
    }
    public void start () {
        if (sensor!=null)  {
            sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME);
        }
    }

    public void stop () {
        sensorManager.unregisterListener(this);
    }

    public void onAccuracyChanged(Sensor s, int valu) {

    }
    public void onSensorChanged(SensorEvent event) {

        if (event.sensor.getType() != Sensor.TYPE_ACCELEROMETER || event.values.length < 3)
              return;

        currentTime = System.currentTimeMillis();

        if ((currentTime - lastUpdate) > 100) {
            long diffTime = (currentTime - lastUpdate);
            lastUpdate = currentTime;

            current_x = event.values[DATA_X];
            current_y = event.values[DATA_Y];
            current_z = event.values[DATA_Z];

            currenForce = Math.abs(current_x+current_y  + current_z - last_x - last_y - last_z) / diffTime * 10000;
           if (currenForce > coefficient)
            {
        	   //Toast.makeText(context, currenForce + "", Toast.LENGTH_SHORT).show();
            	if (currentTime - lastok < 200)
            	{
            		Log.d("AccelerometerListener", "震动啦");
            		//handler.removeMessages(endCall);
                	//handler.sendEmptyMessage(endCall);
            	}
            	lastok = lastUpdate;
            }
           // Log.d("AccelerometerListener", "currenForce: " + currenForce);
            //currenForce = Math.abs(current_x + current_y  + current_z - last_x - last_y - last_z) * 100;
//            if (currenForce > FORCE_THRESHOLD) {
//            	Toast.makeText(context, currenForce + "", Toast.LENGTH_SHORT).show();
//            	Log.d("AccelerometerListener", "currenForce: " + currenForce);
//            	handler.removeMessages(endCall);
//            	handler.sendEmptyMessage(endCall);
//            }
            last_x = current_x;
            last_y = current_y;
            last_z = current_z;

        }
    }

}