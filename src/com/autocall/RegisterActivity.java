package com.autocall;

import com.common.Constant;
import com.utils.Encrytp;
import com.utils.SaveUtil;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class RegisterActivity extends Activity implements OnClickListener {
	
	private TextView txt_machine;
	private EditText register_text;
	private Button register;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		
		txt_machine = (TextView) findViewById(R.id.machine);
		register_text = (EditText) findViewById(R.id.register_text);
		register = (Button) findViewById(R.id.register);
		
		txt_machine.setText(Constant.MACHINE);
		register.setOnClickListener(this);
	}

	 private void showDialog(String text){
		 AlertDialog.Builder builder = new AlertDialog.Builder(this);
		 builder.setTitle("提示");
		 builder.setMessage(text);
		 builder.setPositiveButton("确定", null);
		 builder.show();
	 }
	 
	@Override
	public void onClick(View v) {
		String text = register_text.getText().toString().trim();
		if (text.length() <= 0)
		{
			showDialog("请填写注册码");
			return;
		}
		
		String zcm = Encrytp.getEncrypt(Constant.MACHINE);
		
		if (zcm.equals(text))
		{
			SaveUtil.saveString(this, Constant.REGISTER, "true");
			Intent intent = new Intent(this, MainActivity.class);
			startActivity(intent);
			finish();
		}
		else
		{
			SaveUtil.saveString(this, Constant.REGISTER, "false");
			showDialog("注册失败");
		}
	}
}
