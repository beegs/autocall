package com.autocall;

import java.lang.reflect.Method;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.ITelephony;
import com.bean.CallData;
import com.fragment.CallPhoneFragment;
import com.utils.AccelerometerListener;

public class CallService extends Service
{
	public static interface StatusCallBack{
		public void destory();
	}
	
	enum PhoneStatus{
		OFFHOOK,
		IDLE
	}
	
	private final String TAG = "CallService";
	private ITelephony iTelephony;
	private MyPhoneStateReceiver myPhoneStateReceiver;
	private TelephonyManager manager;
	private PhoneStatus phoneStatus = PhoneStatus.IDLE;
	private CallData callData;
	private AccelerometerListener accelerometerListener;
	private StatusCallBack statusCallBack;
	
	private final int START_CALL = 0;
	private final int STOP_CALL = 1;
	private final int DESTORY_CALL = 2;
	private final int CHECK_STATUS = 3;
	private final int AUTO_STOP_CALL = 4;

	Handler handler = new Handler()
	{
		public void handleMessage(android.os.Message msg)
		{
			if (callData == null)
			{
				return;
			}
			
			switch (msg.what)
			{
			case START_CALL:
				handler.removeMessages(START_CALL);
				handler.removeMessages(STOP_CALL);
				handler.removeMessages(DESTORY_CALL);
				handler.removeMessages(CHECK_STATUS);
				handler.removeMessages(AUTO_STOP_CALL);
				if (callData.type == 1) {
					if (CallPhoneFragment.nums.size() <= 0)
					{
						return;
					}
					callData.phoneNum = CallPhoneFragment.nums.remove(0);
					CallPhoneFragment.nums.add(callData.phoneNum);
				}
				
				Log.d(TAG + "handler", "START_CALL");
				Intent phoneIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + callData.phoneNum));
				phoneIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(phoneIntent);
				if (callData.type == 2){
                    long num = Long.parseLong(callData.phoneNum);
                    callData.phoneNum =  String .format("%d", num + 1);
                }
				break;
			case STOP_CALL:
				if (phoneStatus == PhoneStatus.IDLE)
				{
					return;
				}
				Log.d(TAG + "handler", "STOP_CALL");

                if (callData.type == 2) {
                    long num = Long.parseLong(callData.phoneNum);
                    long endnum = Long.parseLong(callData.endPhoneNum);
                    if (endnum < num) {
                        handler.sendEmptyMessage(DESTORY_CALL);
                        break;
                    }
                }

                try {
					iTelephony.endCall();
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				
				if (callData.isRepeat)
				{
					handler.sendEmptyMessageDelayed(CHECK_STATUS, 1000);
				}
				else
				{
					callData.number -= 1;
					if (callData.number > 0)
					{
						handler.sendEmptyMessageDelayed(CHECK_STATUS, 1000);
                    }
                    else if (statusCallBack != null){
                        statusCallBack.destory();
                    }
				}
				
				handler.removeMessages(AUTO_STOP_CALL);
				handler.removeMessages(STOP_CALL);
				break;
			case DESTORY_CALL:
				callData = null;
				Log.d(TAG + "handler", "DESTORY_CALL");
				try {
					iTelephony.endCall();
				} catch (RemoteException e) {
					e.printStackTrace();
				}

				if (statusCallBack != null)
				{
					statusCallBack.destory();
				}
				handler.removeMessages(START_CALL);
				handler.removeMessages(STOP_CALL);
				handler.removeMessages(DESTORY_CALL);
				handler.removeMessages(CHECK_STATUS);
				handler.removeMessages(AUTO_STOP_CALL);
				break;
			case CHECK_STATUS:
				Log.d(TAG + "handler", "CHECK_STATUS");
				try {
					if (iTelephony.showCallScreen())
					{
						handler.sendEmptyMessageDelayed(CHECK_STATUS, 1000);
					}
					else
					{
						handler.sendEmptyMessageDelayed(START_CALL, callData.interval * 1000);
					}
				} catch (Exception e) {
					e.printStackTrace();
					
					handler.sendEmptyMessageDelayed(START_CALL, (4 + callData.interval) * 1000);
				}
				break;
			case AUTO_STOP_CALL:
				if (phoneStatus == PhoneStatus.IDLE){
					handler.sendEmptyMessage(START_CALL);
				}
				else {
					handler.sendEmptyMessage(STOP_CALL);
				}
				break;
				
			}
		};
	};
	@Override
	public void onCreate()
	{
		super.onCreate();

		myPhoneStateReceiver = new MyPhoneStateReceiver();

		//获取管理器
		manager = (TelephonyManager) this.getSystemService(Service.TELEPHONY_SERVICE);
		
		try
		{
            Method getITelephonyMethod = manager.getClass().getDeclaredMethod("getITelephony");
            getITelephonyMethod.setAccessible(true);
            iTelephony = (ITelephony) getITelephonyMethod.invoke(manager);
		}
		catch (Exception e)
		{
            e.printStackTrace();
		}
		
		registerReceiver();
		
		accelerometerListener = new AccelerometerListener(this, handler, STOP_CALL);
		//accelerometerListener.start();
	}
	
	private void registerReceiver()
	{
		Log.i(TAG, "成功注册广播");
		IntentFilter intentFilter = new IntentFilter();  
	    intentFilter.addAction("android.intent.action.PHONE_STATE");
	    intentFilter.addAction("android.intent.action.NEW_OUTGOING_CALL");
	    intentFilter.setPriority(Integer.MAX_VALUE);
	    registerReceiver(myPhoneStateReceiver, intentFilter);  
	}
	
	private void unregisterReceiver()
	{
		 unregisterReceiver(myPhoneStateReceiver);  
	}
	
	public class MyBinder extends Binder{
        public CallService getService(){
            return CallService.this;
        }
    }
	    
    private MyBinder myBinder = new MyBinder();
	    
	@Override
	public IBinder onBind(Intent intent)
	{
		return myBinder;
	}
	
	@Override
	public void onDestroy()
	{
        destoryCall();
		accelerometerListener.stop();
		unregisterReceiver();
		super.onDestroy();
	}

	public void setStatusCallBack(StatusCallBack statusCallBack)
	{
		this.statusCallBack = statusCallBack;
	}
	
	public void startCall(CallData callData){
		this.callData = callData;
		handler.sendEmptyMessage(START_CALL);
	}
	
	public void destoryCall()
	{
		handler.sendEmptyMessage(DESTORY_CALL);
	}
	
	private class MyPhoneStateReceiver extends BroadcastReceiver
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			String action = intent.getAction();  
	        Log.i(TAG, "[Broadcast]"+action);  
			
	        doReceivePhone(context, intent);
		}
		
		  /** 
	     * 处理电话广播. 
	     * @param context 
	     * @param intent 
	     */  
	    public void doReceivePhone(Context context, Intent intent)
	    {  
	        String phoneNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);  
	        TelephonyManager telephony = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);  
	        int state = telephony.getCallState();  
	          
	        switch(state)
	        {  
	        case TelephonyManager.CALL_STATE_RINGING:  
	            Log.i(TAG, "[Broadcast]等待接电话="+phoneNumber);  
	            break;  
	        case TelephonyManager.CALL_STATE_IDLE:  
	        	phoneStatus = PhoneStatus.IDLE;
	            Log.i(TAG, "[Broadcast]电话挂断="+phoneNumber);  
	            //handler.removeMessages(AUTO_STOP_CALL);
	            break;  
	        case TelephonyManager.CALL_STATE_OFFHOOK:  
	        	phoneStatus = PhoneStatus.OFFHOOK;
	            Log.i(TAG, "[Broadcast]通话中="+phoneNumber);  
	            if (callData != null)
	            handler.sendEmptyMessageDelayed(AUTO_STOP_CALL, callData.delay * 1000);
	            break;  
	        }  
	    }

	}
}