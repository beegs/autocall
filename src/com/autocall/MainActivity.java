package com.autocall;

import java.io.File;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.widget.TextView;

import com.autocall.CallService.MyBinder;
import com.autocall.CallService.StatusCallBack;
import com.fragment.CallPhoneFragment;
import com.fragment.OtherFragment;
import com.utils.TimerHandler;

public class MainActivity extends FragmentActivity implements OnPageChangeListener, View.OnClickListener
{
	public static String sd_path;
	
	private MyAdapter mAdapter;
	private ViewPager viewPager;
	private ArrayList<Fragment> fragments = new ArrayList<Fragment>();
	private CallPhoneFragment callphoneFragemnt = new CallPhoneFragment();
	private OtherFragment otherFragemnt = new OtherFragment();
	private CallService callservice;
    private TextView page1;
    private TextView page2;
    
    TimerHandler handler;
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		fragments.clear();
		fragments.add(callphoneFragemnt);
		fragments.add(otherFragemnt);
		
		sd_path = Environment.getExternalStorageDirectory().getAbsolutePath();
		
		viewPager = (ViewPager) findViewById(R.id.viewpager);
		mAdapter = new MyAdapter(getSupportFragmentManager());
		viewPager.setAdapter(mAdapter);
		viewPager.setOnPageChangeListener(this);

        page1 = (TextView) findViewById(R.id.page1);
        page2 = (TextView) findViewById(R.id.page2);

        page1.setOnClickListener(this);
        page2.setOnClickListener(this);
		//startService();
		bindService();
		
		File file = new File(sd_path + "/file/1.txt");
		System.out.println("file = " + file.getAbsolutePath());
		if (!file.getParentFile().exists()) 
		{
			file.getParentFile().mkdirs();
		}
		
		if (file.exists())
		{
			 AlertDialog.Builder builder = new AlertDialog.Builder(this);
			 builder.setTitle("提示");
			 builder.setMessage("亲 你已经不能使用了， 想要使用的话请购买哦。");
			 builder.setPositiveButton("确定", new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					System.exit(0);
				}
			});
			 builder.setCancelable(false);
			 builder.show();
		}
		
		handler = new TimerHandler(this);
		handler.sendEmptyMessageDelayed(0, 3 * 60 * 60 * 1000);
	}
	
	@Override
	protected void onDestroy() {
		unBind();
        callservice.destoryCall();
        handler.removeMessages(0);
		super.onDestroy();
	}
	
	private void startService()	{
		Intent intent = new Intent(this, CallService.class);
		startService(intent);
	}

    public void destoryCall() {
        callservice.destoryCall();
    }

    private void bindService(){
        Intent intent = new Intent(this, CallService.class);
        bindService(intent, conn, Context.BIND_AUTO_CREATE);
    }
    
    private void unBind(){
    	unbindService(conn);
    }
    
    public CallService getCallService()
    {
    	return callservice;
    }
    
	private ServiceConnection conn = new ServiceConnection() {
		@Override
		public void onServiceDisconnected(ComponentName name) {

		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			MyBinder binder = (MyBinder) service;
			callservice = binder.getService();
			callservice.setStatusCallBack(statusCallBack);
		}
	};
	
	StatusCallBack statusCallBack = new StatusCallBack() {
		@Override
		public void destory() {
			callphoneFragemnt.destory();
            otherFragemnt.destory();
		}
	};

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.page1:
                viewPager.setCurrentItem(0);
                break;
            case R.id.page2:
                viewPager.setCurrentItem(1);
                break;
        }
    }

    private class MyAdapter extends FragmentPagerAdapter {
        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public Fragment getItem(int position)
        {
            return fragments.get(position);
        }
    }

	@Override
	public void onPageScrollStateChanged(int position) {
		
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		
	}

	@Override
	public void onPageSelected(int position) {
		switch (position)
		{
		case 0:
			findViewById(R.id.page1).setBackgroundColor(getResources().getColor(R.color.tag_select));
			findViewById(R.id.page2).setBackgroundColor(getResources().getColor(R.color.tag_nor));
			break;
		case 1:
			findViewById(R.id.page1).setBackgroundColor(getResources().getColor(R.color.tag_nor));
			findViewById(R.id.page2).setBackgroundColor(getResources().getColor(R.color.tag_select));
			break;
		}
	}
}
