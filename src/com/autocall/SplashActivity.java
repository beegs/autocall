package com.autocall;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings.Secure;

import com.common.Constant;
import com.utils.SaveUtil;

public class SplashActivity extends Activity
{
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		Constant.MACHINE = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);
		if (Constant.MACHINE.length() > 7)
		{
			Constant.MACHINE = Constant.MACHINE.substring(0,  7);
		}
		
		// 临时
		String register = SaveUtil.readString(this, Constant.REGISTER);
		
		if (register == null || register.equals("false"))
		{
			Intent intent = new Intent(this, RegisterActivity.class);
			startActivity(intent);;
		}
		else
		{
			Intent intent = new Intent(this, MainActivity.class);
			startActivity(intent);
		}
		
		finish();
	}
}
