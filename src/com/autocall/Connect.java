package com.autocall;

import android.content.Context;
import android.util.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by shenc on 14-5-30.
 */
public class Connect {

    public static boolean isCallConnected() {
        boolean isAlive = false;


        try {
            Class connectionClass = Class
                    .forName("com.android.internal.telephony.Connection");
            Method isAliveMethod = connectionClass.getDeclaredMethod("isAlive",
                    (Class[]) null);


            /*
            * Method isAliveMethod =
            * connectionClass.getDeclaredMethod("isRinging", (Class[]) null);
            */

            isAliveMethod.setAccessible(true);
            Object connection = connectionClass.newInstance();
            try {
                isAlive = (Boolean) isAliveMethod.invoke(connection, (Object[]) null);


            } catch (IllegalArgumentException e) {
                // TODO Auto-generated catch block
                Log.e("song", "song:illegal argument");
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                // TODO Auto-generated catch block
                Log.e("song", "song:" + e.getMessage());
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                // TODO Auto-generated catch block
                Log.e("song", "song:" + e.getMessage());
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            Log.e("song", "song: class not found");
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            // TODO Auto-generated catch block
            Log.e("song", "song:no such method");
            e.printStackTrace();
        } catch (InstantiationException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (IllegalAccessException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        return isAlive;
    }
    
    public static boolean isAct(Context context)
    {
    	try {
    		ClassLoader classLoader = context.getClass().getClassLoader();
    		final Class<?> callManagerClass = classLoader.loadClass("com.android.internal.telephony.CallManager");
    		 
    		Method getInstanceMethod = callManagerClass.getDeclaredMethod("getInstance");
    		getInstanceMethod.setAccessible(true);
    		Object mCallManager = getInstanceMethod.invoke(null);
    		 
    		Method mActiveFgMethod = callManagerClass.getDeclaredMethod("hasActiveFgCall");
    		Object res = mActiveFgMethod.invoke(mCallManager);
    		
        	return ((Boolean) res).booleanValue();
		} catch (Exception e) {
			// TODO: handle exception
		}
    	
    	return false;
    }
}
